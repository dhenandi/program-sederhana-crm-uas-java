/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileClass;

/**
 *
 * @author dhenandi
 */
public class comboTech {
    
    private int TechNum;
    private String TechName;
    
    public comboTech(int techNum, String techName){
        this.TechNum = techNum;
        this.TechName = techName;
    }

    public int getTechNum() {
        return TechNum;
    }

    public void setTechNum(int TechNum) {
        this.TechNum = TechNum;
    }

    public String getTechName() {
        return TechName;
    }

    public void setTechName(String TechName) {
        this.TechName = TechName;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileClass;

/**
 *
 * @author dhenandi
 */
public class comboProduct {
    
    private int ProdNum;
    private String ProdName;
    
    public comboProduct(int prodNum, String prodName){
        this.ProdNum = prodNum;
        this.ProdName = prodName;
    }

    public int getProdNum() {
        return ProdNum;
    }

    public void setProdNum(int ProdNum) {
        this.ProdNum = ProdNum;
    }

    public String getProdName() {
        return ProdName;
    }

    public void setProdName(String ProdName) {
        this.ProdName = ProdName;
    }
    
    
    
}

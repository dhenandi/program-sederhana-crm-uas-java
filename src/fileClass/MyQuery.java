/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileClass;

import fileClass.comboBill;
import fileClass.comboProduct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.Query;
/**
 *
 * @author dhenandi
 */
public class MyQuery {
    
    public Connection getConnection(){
        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:8889/smt4","root","root");
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
    
    public HashMap<String, Integer> populateProvinceCombo(){
      HashMap<String, Integer> map = new HashMap<String, Integer>();
      Connection con = getConnection();
      Statement st;
      ResultSet rs;
      
       try {
           st = con.createStatement();
           rs = st.executeQuery("select * from province");
           comboItem cmi;
           
           while(rs.next()){
               cmi = new comboItem(rs.getInt(1), rs.getString(2));
               map.put(cmi.getCatName(), cmi.getCatId());
           }
           
       } catch (SQLException ex) {
           Logger.getLogger(MyQuery.class.getName()).log(Level.SEVERE, null, ex);
       }
      
       return map;
    }
    
    public HashMap<String, Integer> populateProductCombo(){
      HashMap<String, Integer> map = new HashMap<String, Integer>();
      Connection con = getConnection();
      Statement st;
      ResultSet rs;
      
       try {
           st = con.createStatement();
           rs = st.executeQuery("select product_id,product_name from product");
           comboProduct cmi;
           
           while(rs.next()){
               cmi = new comboProduct(rs.getInt(1), rs.getString(2));
               map.put(cmi.getProdName(), cmi.getProdNum());
           }
           
       } catch (SQLException ex) {
           Logger.getLogger(MyQuery.class.getName()).log(Level.SEVERE, null, ex);
       }
      
       return map;
    }
    
    
    
    public HashMap<String, Integer> populateTechnicalCombo(){
      HashMap<String, Integer> map = new HashMap<String, Integer>();
      Connection con = getConnection();
      Statement st;
      ResultSet rs;
      
       try {
           st = con.createStatement();
           rs = st.executeQuery("select contact_id,contact_name from contact where contact_type='Technical'");
           comboTech cth;
            
           while(rs.next()){
               cth = new comboTech(rs.getInt(1), rs.getString(2));
               map.put(cth.getTechName(), cth.getTechNum());
           }
           
       } catch (SQLException ex) {
           Logger.getLogger(MyQuery.class.getName()).log(Level.SEVERE, null, ex);
       }
      
       return map;
   }
    
   public HashMap<String, Integer> populateBillingCombo(){
      HashMap<String, Integer> map = new HashMap<String, Integer>();
      Connection con = getConnection();
      Statement st;
      ResultSet rs;
      
       try {
           st = con.createStatement();
           rs = st.executeQuery("select contact_id,contact_name from contact where contact_type='Billing'");
           comboBill cbl;
            
           while(rs.next()){
               cbl = new comboBill(rs.getInt(1), rs.getString(2));
               map.put(cbl.getBillName(), cbl.getBillNum());
           }
           
       } catch (SQLException ex) {
           Logger.getLogger(MyQuery.class.getName()).log(Level.SEVERE, null, ex);
       }
      
       return map;
   } 
    
}

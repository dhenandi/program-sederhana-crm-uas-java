/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileClass;

import java.awt.HeadlessException;
import static java.lang.Class.forName;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.Query;
import javax.xml.transform.Result;
import javax.swing.JOptionPane;
/**
 *
 * @author dhenandi
 */
public class dbConnect {
    Connection cnn;
    Statement stmt;
    ResultSet rs;
    PreparedStatement st;
    
    public dbConnect() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
        } catch(ClassNotFoundException e){
            JOptionPane.showMessageDialog(null, "error inisialisasi Class: "+ e);
        }
        
        try{
            stmt=DriverManager.getConnection("jdbc:mysql://localhost:8889/smt4","root","root").createStatement();
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "error koneksi ke database: "+ e);
        }
    }
    
    public ResultSet lihatData(String sql){
       try{
           rs=stmt.executeQuery(sql);
       }catch(SQLException e){
           JOptionPane.showMessageDialog(null, "Error Query : "+ e);
       }
       return rs;
    }    
      
    
    public static void main(String[] args) {
        try{
            dbConnect cn = new dbConnect();
            JOptionPane.showMessageDialog(null, "Koneksi Sukses");
        } catch(HeadlessException e){
            JOptionPane.showMessageDialog(null, "Koneksi Gagal"+ e);
        }
    }
    
    public void dmlData(String sql){
        try{
            stmt.executeUpdate(sql);
        }catch(SQLException e){
            System.out.println("Error");
        }
    }
    
    public static Connection getConnect() throws ClassNotFoundException {
        Connection connection = null;
        if (connection == null) {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:8889/smt4",
            "root",
            "root");
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null,"error koneksi ke database : "+ e);
        }
        }
        return connection;
    }
    
//    public Connection getConnection(){
//        Connection con = null;
//        try {
//            con = DriverManager.getConnection("jdbc:mysql://localhost:8889/smt4","root","root");
//        } catch (SQLException ex) {
//            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return con;
//    }
    
//    public HashMap<String, Integer> populateCombo(){
//      HashMap<String, Integer> map = new HashMap<String, Integer>();
//      Connection con = getConnection();
//      Statement st;
//      ResultSet rs;
//      
//       try {
//           st = con.createStatement();
//           rs = st.executeQuery("SELECT `province_id`, `province_name` FROM `province`");
//           comboItem cmi;
//           
//           while(rs.next()){
//               cmi = new comboItem(rs.getInt(1), rs.getString(2));
//               map.put(cmi.getCatName(), cmi.getCatId());
//           }
//           
//       } catch (SQLException ex) {
//           Logger.getLogger(comboItem.class.getName()).log(Level.SEVERE, null, ex);
//       }
//      
//       return map;
//   }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileClass;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dhenandi
 */
public class UNUSEDKoneksi {
    MysqlDataSource dataSource=new MysqlDataSource();
 
    public UNUSEDKoneksi() {
        dataSource.setUser("root");
        dataSource.setPassword("root");
        dataSource.setServerName("localhost");
        dataSource.setPort(8889);
        dataSource.setDatabaseName("smt4");
    }
    
    public Connection getConnection(){
        try {
            return dataSource.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(UNUSEDKoneksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileClass;

/**
 *
 * @author dhenandi
 */
public class comboBill {
    
    private int BillNum;
    private String BillName;
    
    public comboBill(int billNum, String billName){
        this.BillNum = billNum;
        this.BillName = billName;
    }

    public int getBillNum() {
        return BillNum;
    }

    public void setBillNum(int BillNum) {
        this.BillNum = BillNum;
    }

    public String getBillName() {
        return BillName;
    }

    public void setBillName(String BillName) {
        this.BillName = BillName;
    }
    
}

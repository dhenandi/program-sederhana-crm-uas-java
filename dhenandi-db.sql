-- MySQL dump 10.13  Distrib 5.6.35, for osx10.9 (x86_64)
--
-- Host: localhost    Database: smt4
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `contact_id` int(10) NOT NULL AUTO_INCREMENT,
  `contact_phone` varchar(100) NOT NULL,
  `contact_name` varchar(100) DEFAULT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  `contact_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'082125141324','Muhammad Dhenandi','dhenandi@excellent.co.id','Technical'),(2,'08293232323','Rizal Pratama','pratama@pratama.biz.id','Billing'),(3,'089630483294','Hasan','dhenandi@excellent.co.id','Technical'),(7,'0896896896','Ahmad Sanewsi','ahmadsanusi@gmail.com','Billing'),(9,'0895304932323','Choirrulloh','choirrulloh@choirrulloh.web.id','Billing');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customer_id` int(100) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) NOT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `company_state` varchar(100) DEFAULT NULL,
  `province_id` int(100) NOT NULL,
  `product_id` int(10) NOT NULL,
  `subscription_name` varchar(100) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `contact_it` int(10) NOT NULL,
  `contact_billing` int(10) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `customer_fk1` (`province_id`),
  KEY `customer_fk2` (`product_id`),
  KEY `customer_fk3` (`contact_it`),
  KEY `customer_fk4` (`contact_billing`),
  CONSTRAINT `customer_fk1` FOREIGN KEY (`province_id`) REFERENCES `province` (`province_id`),
  CONSTRAINT `customer_fk2` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `customer_fk3` FOREIGN KEY (`contact_it`) REFERENCES `contact` (`contact_id`),
  CONSTRAINT `customer_fk4` FOREIGN KEY (`contact_billing`) REFERENCES `contact` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'PT Aktiva Kreasi Investama','Duren Jaya','Bekasi',23,2,'Monthly','2019-07-10','2019-08-10',1,7,'Active'),(2,'PT Raisa Mencari Cinta','Pengasinan','Bekasi',33,6,'Monthly','2019-07-11','2019-08-11',3,2,'Inactive'),(4,'PT Maju Terus Pantang Mundur','Ini adalah alamat','Semarang',13,2,'Monthly','2019-07-04','2019-08-04',3,7,'Active');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `display_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES ('dhenandi','*3E5287812B7D1F947439AC45E73935377A3ADEF7','m@putra.com','Muhammad Putra');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  `price` int(100) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Excellent Managed Services Bronze',1000000,'RAM 2 GB, dll'),(2,'Excellent Managed Services Gold',2000000,'ini adalah paket gold\ndengan ram 3 gb \ndan lain lain'),(3,'Excellent SMTP Relay Bronze',200000,'SMTP Relay Maks: 100'),(4,'Excellent SMTP Relay Silver',300000,'1\n'),(6,'Lisensi Untangle Home Edition',500000,'Mencakup:\n\nSupport\nWeb Filter\nWeb Block\nProxy\nLisensi\nSertifikat\nSSL Inspector\nWeb Cache\nWeb Strict'),(7,'Lisensi VMware Professional Edition',100000000,'Lisensi VMware Proffessional Edition:\n\nInclude:\n\n- 2 Socket processor\n- vCenter');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province` (
  `province_id` int(100) NOT NULL AUTO_INCREMENT,
  `province_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province`
--

LOCK TABLES `province` WRITE;
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
INSERT INTO `province` VALUES (1,'Aceh'),(2,'Sumatera Utara'),(3,'Sumatera Barat'),(4,'Riau'),(5,'Jambi'),(6,'Sumatera Selatan'),(7,'Bengkulu'),(8,'Lampung'),(9,'Kepulauan Bangka Belitung'),(10,'Kepulauan Riau'),(11,'Dki Jakarta'),(12,'Jawa Barat'),(13,'Jawa Tengah'),(14,'Di Yogyakarta'),(15,'Jawa Timur'),(16,'Banten'),(17,'Bali'),(18,'Nusa Tenggara Barat'),(19,'Nusa Tenggara Timur'),(20,'Kalimantan Barat'),(21,'Kalimantan Tengah'),(22,'Kalimantan Selatan'),(23,'Kalimantan Timur'),(24,'Kalimantan Utara'),(25,'Sulawesi Utara'),(26,'Sulawesi Tengah'),(27,'Sulawesi Selatan'),(28,'Sulawesi Tenggara'),(29,'Gorontalo'),(30,'Sulawesi Barat'),(31,'Maluku'),(32,'Maluku Utara'),(33,'Papua Barat'),(34,'Papua');
/*!40000 ALTER TABLE `province` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-24  9:48:04
